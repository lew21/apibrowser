# APIBrowser

Generic GUI for any RESTful API.

See the demo at <https://apibrowser-demo.herokuapp.com/> ([source](https://gitlab.com/lew21/demoapi)).

## Usage

To serve APIBrowser as the GUI of your API, you need to check the Accept header on each GET request. If it contains text/html, respond with:

``` html
<!DOCTYPE html><html><head><meta charset=utf-8><meta name=viewport content="width=device-width,initial-scale=1"><title>APIBrowser</title><link href=https://apibrowser.storage.googleapis.com/static/css/app.568f5941d42567ef2a65c5cdb7b32463.css rel=stylesheet integrity="sha256-wu9idIWIq6GpemTy2UjJyALKwWzhx3asHU+SG2udyEE=" crossorigin=anonymous></head><body><div id=app></div><script type=text/javascript src=https://apibrowser.storage.googleapis.com/static/js/app.9f8edfd9b0e1afb81473.js integrity="sha256-Sdgc+hP8zDdwQwDYypvPePGGs3m7Auqw9Qs9uxOwCoQ=" crossorigin=anonymous></script></body></html>
```

And that's it!

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
